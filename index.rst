
.. raw:: html

   <a rel="me" href="https://qoto.org/@rojavaluttes">Qoto</a>


.. _linkertree_rojavaluttes:

=====================================================================
Liens **rojavaluttes**
=====================================================================


Site rojavaluttes
==========================

- :ref:`Luttes au rojava <rojava_luttes:rojava_luttes>`

.. figure:: images/site_rojava_luttes.png
   :align: center

   :ref:`Luttes au Rojava <rojava_luttes:rojava_luttes>`

rojavaluttes sur Mastodon https://qoto.org/@rojavaluttes
=============================================================

- https://qoto.org/@rojavaluttes
- @rojavaluttes@qoto.org

.. figure:: images/qoto.png
   :align: center

   https://qoto.org/@rojavaluttes


Flux Web RSS
--------------

- https://paris-luttes.info/spip.php?page=backend&id_mot=711
- https://qoto.org/@rojavaluttes.rss

Exemples de tags RSS

- https://framapiaf.org/web/tags/MahsaAmini.rss
- https://framapiaf.org/web/tags/iran.rss
- https://framapiaf.org/web/tags/kurdistan.rss
- https://framapiaf.org/web/tags/rojava.rss


rojavaluttes sur mobilizon
=====================================

- https://mobilizon.chapril.org/@rojavaluttes

.. figure:: images/mobilizon.png
   :align: center

   https://mobilizon.chapril.org/@rojavaluttes38



